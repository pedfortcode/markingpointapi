var express = require('express');
var router = express.Router();
var markings = require('../controllers/markingController');

/**
 * @swagger
 * definitions:
 *   Marking:
 *     properties:
 *       id:
 *         type: integer
 *       type:
 *         type: string
 *       createdin:
 *         type: integer
 */

 /**
 * @swagger
 * /markings:
 *   get:
 *     tags:
 *       - Markings
 *     description: Returns all markings
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: An array of markings
 *         schema:
 *           $ref: '#/definitions/Marking'
 */
router.get('/markings', markings.list_all_markings);

/**
 * @swagger
 * /markings:
 *   post:
 *     tags:
 *       - Markings
 *     description: Creates a new marking
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: marking
 *         description: Marking object
 *         in: body
 *         required: true
 *         schema:
 *           $ref: '#/definitions/Marking'
 *     responses:
 *       200:
 *         description: Successfully created
 */
router.post('/markings', markings.create_a_marking);

/**
 * @swagger
 * /markings/{markingId}:
 *   get:
 *     tags:
 *       - Markings
 *     description: Returns a single Marking
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: markingId
 *         description: Marking id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: A single Marking
 *         schema:
 *           $ref: '#/definitions/Marking'
 */
router.get('/markings/:markingId', markings.read_a_marking);

/**
 * @swagger
 * /markings/{markingId}:
 *   delete:
 *     tags:
 *       - Markings
 *     description: Delete a Marking by Id
 *     produces:
 *       - application/json
 *     parameters:
 *       - name: markingId
 *         description: Marking id
 *         in: path
 *         required: true
 *         type: integer
 *     responses:
 *       200:
 *         description: Sucess exclude
 */
router.delete('/markings/:markingId', markings.delete_a_marking);

module.exports = router;
