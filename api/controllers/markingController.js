/* eslint-disable func-names */
/* eslint-disable camelcase */
/* eslint-disable no-console */
const Marking = require('../models/markingModel');
const SqlLite = require('../helpers/sqlite');

const sqlite = new SqlLite('MarkingPointDB.db');

exports.list_all_markings = function (req, res) {
  const listM = sqlite.getMarkings();
  res.json(listM);
};
exports.create_a_marking = function (req, res) {
  const new_marking = new Marking(req.body);
  new_marking.id = undefined;
  const r = sqlite.addMarking(new_marking);
  if (Number(r)) {
    new_marking.id = r; 
    res.json(new_marking);
  } else {
    res.send(r);
  }
};
exports.read_a_marking = function (req, res) {
  res.send(req.params);
};
exports.update_a_marking = function (req, res) {
  res.send(req.params);
};
exports.delete_a_marking = function (req, res) {
  const r = sqlite.delMarking(Number(req.params.markingId));
  res.json(r);
};
