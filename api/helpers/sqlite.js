const sqlite = require('sqlite-sync');

const tableMarkingsName = 'Markings';

class Sqlite {

  constructor(database) {
    this.database = database;
    this.createTable(tableMarkingsName, 'ID  INTEGER PRIMARY KEY AUTOINCREMENT, TYPE TEXT NOT NULL, CREATEDIN INTEGER NOT NULL');
  }

  tableExists(name) {
    sqlite.connect(this.database);
    const rows = sqlite.run(`SELECT * FROM sqlite_master WHERE name=${name}`);
    return rows !== undefined && rows > 0;
  }

  createTable(name, fields) {
    sqlite.connect(this.database);
    sqlite.run(`CREATE TABLE IF NOT EXISTS ${name} (${fields})`);
  }

  getMarkings() {
    sqlite.connect(this.database);
    const rows = sqlite.run(`SELECT * FROM ${tableMarkingsName}`);
    return rows;
  }

  addMarking(marking) {
    sqlite.connect(this.database);
    const rows = sqlite.insert(tableMarkingsName, marking);
    return rows;
  }

  delMarking(idMarking) {
    sqlite.connect(this.database);
    const rows = sqlite.run(`DELETE FROM ${tableMarkingsName} WHERE ID = ${idMarking}`);
    return rows;
  }

}
module.exports = Sqlite;
