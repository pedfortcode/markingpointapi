/* eslint-disable no-var */
/**
 * Module dependencies.
 */
var express = require('express');
var bodyParser = require('body-parser');
var swaggerJSDoc = require('swagger-jsdoc');
var routes = require('./api/routes/markingRoutes');
var path = require('path');

var portIn = 3000;

var app = express();
var port = process.env.PORT || portIn;
var swgHost = port === portIn ? `localhost:${port}` : 'markingpoint.azurewebsites.net';


// swagger definition
var swgDefinition = {
  info: {
    title: 'Node Swagger API',
    version: '1.0.0',
    description: 'Demonstrating how to describe a RESTful API with Swagger',
  },
  host: swgHost,
  basePath: '/',
};

// options for the swagger docs
var options = {
  // import swaggerDefinitions
  swaggerDefinition: swgDefinition,
  // path to the API docs
  apis: ['./api/routes/*.js'],
};

// initialize swagger-jsdoc
var swaggerSpec = swaggerJSDoc(options);

app.use(express.static(path.join(__dirname, 'public')));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use('/', routes);

// eslint-disable-next-line func-names
app.get('/swagger.json', (req, res) => {
  if (swaggerSpec.paths !== undefined && swaggerSpec.paths.definitions !== undefined) {
    if (swaggerSpec.definitions !== undefined) {
      swaggerSpec.definitions = swaggerSpec.paths.definitions;
    }
  }
  res.setHeader('Content-Type', 'application/json');
  res.send(swaggerSpec);
});

// define a simple route
app.get('/', (req, res) => {
  res.json({ message: 'Api for MarkingPoint' });
});

// listen for requests


app.listen(port, () => {
  // eslint-disable-next-line no-console
  console.log(`Server is listening on port ${port}`);
});
